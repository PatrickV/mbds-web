const fs = require('fs');
const ejs = require('ejs');
const path = require('path');
const sharp = require('sharp');
const uuid = require('uuid/v4');
const bcrypt = require('bcrypt');
const assert = require('assert');
const multer = require('multer');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const paginate = require('express-paginate');

var Plugin = require('./models/plugin')
var Author = require('./models/author')
var upload = require('./config/multer.config.js');
var ObjectId = require('mongoose').Types.ObjectId;

var port = process.env.PORT || 3000;
var url = process.env.MONGOLAB_URI || 'mongodb://localhost/moddevices'


var app = express();

var db = mongoose.connect(url, {
  useCreateIndex: true,
  useNewUrlParser: true
});

app.set('pages', path.join(__dirname, 'views/pages'));
app.set('partials', path.join(__dirname, 'views/partials'));
app.set('view engine', 'ejs');

app.use(session({
  genid: (req) => {
    return uuid();
  },
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  res.locals.user = req.session.userId;
  res.locals.username = req.session.userName;
  next();
});

function restrict(req, res, next) {
  if (req.session.userId) {
    next();
  } else {
    req.session.error = 'Access denied!';
    res.redirect('/');
  }
}

app.get('/', function (req, res) {
  res.redirect('/gallery');
});

app.post('/login', function (req, res) {
  if (req.body.logusername && req.body.logpassword) {
    Author.findOne({ name: req.body.logusername }, function (err, author) {
      if (err) {
        throwError(res, err);
      }
      if (author !== null) {
        author.comparePassword(req.body.logpassword, function (err, result) {
          if (err) {
            throwError(res, err);
          }
          if (result) {
            req.session.userId = author._id;
            req.session.userName = author.name;
            res.redirect('/gallery');
          }
        });
      } else {
        var err = new Error('Author not found.');
        err.status = 404;
        throwError(res, err);
      }
    });
  } else {
    var err = new Error('All fields required.');
    err.status = 400;
    throwError(res, err);
  }
});

app.post('/register', function (req, res) {
  if (req.body.regusername && req.body.regpassword && req.body.regurl) {
    var author = {
      name: req.body.regusername,
      password: req.body.regpassword,
      url: req.body.regurl
    };
    Author.create(author, function (err, author) {
      if (err) {
        throwError(res, err);
      } else {
        req.session.userId = author._id;
        req.session.userName = author.name;
        res.redirect('/gallery');
      }
    });
  } else {
    var err = new Error('All fields required.');
    err.status = 400;
    res.render('pages/error', {title: 'Erreur', active: 'error', message: err, error: {}});
  }
});

app.get('/logout', function (req, res) {
  if (req.session) {
    req.session.destroy(function (err) {
      if(err) {
        res.render('pages/error', {title: 'Erreur', active: 'error', message: err, error: {}});
      } else {
        res.redirect('/gallery');
      }
    });
  }
});

app.get('/plugin/my-plugin-list/:added*?', restrict, function (req, res) {
  Author.findOne({ _id: new ObjectId(req.session.userId)}, function (err, author) {
    if (err) {
      throwError(res, err);
    }
    if (author.plugins.length > 0) {
      Plugin.find({_id: {$in: author.plugins}}, function (err, plugins) {
        if (err) {
          throwError(res, err);
        }
        res.render('pages/list', {title: 'My plugins', active: 'my-plugins', plugins: plugins});
      });
    } else {
      res.render('pages/list', {title: 'My plugins', active: 'my-plugins', plugins: 'none'});
    }
  });
});

app.get('/plugin/remove/:plugin', restrict, function (req, res) {
  Author.findOne({ _id: new ObjectId(req.session.userId)}, function (err, author) {
    if (err) {
      throwError(res, err);
    }
    Plugin.findOneAndDelete({
      author: req.session.userName,
      _id: new ObjectId(req.params.plugin)}, function (err, plugin) {
        if (err) {
          throwError(res, err);
        }
        var index = author.plugins.indexOf(plugin._id);
        if (index !== -1) author.plugins.splice(index, 1);
        author.save(function (err, updatedAuthor) {
          if (err) {
            throwError(res, err);
          }
          var filename = "public/images/plugins/" + plugin.image;
          var tempFile = fs.openSync(filename, 'r');
          fs.closeSync(tempFile);
          fs.unlinkSync(filename);
          res.redirect('/gallery');
        });
    });
  });
});

app.get('/plugin/edit/:plugin', restrict, function (req, res) {
  Plugin.findOne({author: req.session.userName, _id: new ObjectId(req.params.plugin)}, function (err, plugin) {
    if (err) {
      throwError(res, err);
    }
    res.render('pages/edit', {title: 'Edit plugin', active: 'my-plugins', plugin: plugin});
  });
});

app.post('/plugin/edit/:plugin', upload.single('pluimage'), function (req, res) {
  Author.findOne({ _id: new ObjectId(req.session.userId)}, function (err, author) {
    if (err) {
      throwError(res, err);
    }
    Plugin.findOne({author: req.session.userName, _id: new ObjectId(req.params.plugin)}, function (err, plugin) {
      if (err) {
        throwError(res, err);
      }

      if (req.file !== undefined) {
        sharp('public/images/plugins/' + req.file.filename)
        .resize(600, 450)
        .toBuffer(function (err, buffer) {
          fs.writeFile('public/images/plugins/' + req.file.filename, buffer, function (err) {
            if (err) {
              throwError(res, err);
            }
          });
        });
      }

      var rows = [];
      for (var i = 0; i < req.body.specsControl.length; i++) {
        var row = {
          control: req.body.specsControl[i],
          default: req.body.specsDefault[i],
          min: req.body.specsMin[i],
          max: req.body.specsMax[i]
        }
        rows.push(row);
      }

      plugin.name = req.body.pluname;
      plugin.author_link = author.url;
      if (req.file !== undefined) {
        plugin.image = req.file.filename;
      }
      plugin.desc_rows = rows;
      plugin.version = req.body.pluversion;
      plugin.inspired_by = req.body.pluinspired;
      plugin.desc = req.body.pludesc;
      plugin.tags = req.body.plutags;

      plugin.save(function (err, updatedPlugin) {
        if (err) {
          throwError(res, err);
        }
        res.redirect("/plugin/" + updatedPlugin._id + "/updated");
      });
    });
  });
});

app.get('/plugin/add', restrict, function (req, res) {
  res.render('pages/add', {title: 'Add plugin', active: 'add-plugin'});
});

app.post('/plugin/add', upload.single('pluimage'), function (req, res) {
  Author.findOne({ _id: new ObjectId(req.session.userId) }, function (err, author) {
    if (err) {
      throwError(res, err);
    }
    var rows = [];
    for (var i = 0; i < req.body.specsControl.length; i++) {
      var row = {
        control: req.body.specsControl[i],
        default: req.body.specsDefault[i],
        min: req.body.specsMin[i],
        max: req.body.specsMax[i]
      }
      rows.push(row);
    }

    sharp('public/images/plugins/' + req.file.filename)
    .resize(600, 450)
    .toBuffer(function (err, buffer) {
      fs.writeFile('public/images/plugins/' + req.file.filename, buffer, function (err) {
        if (err) {
          throwError(res, err);
        }
      });
    });

    var plug = {
      name: req.body.pluname,
      author_link: author.url,
      image: req.file.filename,
      author: author.name,
      usage: 0,
      desc_rows: rows,
      version: req.body.pluversion,
      inspired_by: req.body.pluinspired,
      desc: req.body.pludesc,
      tags: req.body.plutags
    }

    Plugin.create(plug, function (err, plugin) {
      if (err) {
        throwError(res, err);
      } else {
        author.plugins.push(plugin._id);
        author.save(function (err, updatedAuthor) {
          if (err) {
            throwError(res, err);
          }
          res.redirect('/plugin/my-plugin-list/added');
        });
      }
    });
  });
});

app.get('/tag/:tag', function (req, res) {
  var tag = req.params.tag;

  Plugin.find({tags: tag}, function (err, plugins) {
    if (err) {
      throwError(res, err);
    }
    res.render('pages/list', {title: tag, active: 'tag', plugins: plugins, tag: tag});
  });
});

app.get('/gallery/:created*?', function (req, res) {
  var message = '';
  if (req.params.created !== undefined && req.params.created === 'true') {
    var message = 'created';
  }
  Plugin
  .find({})
  .sort({'_id': -1})
  .limit(9)
  .exec(function (err, plugins) {
    if (err) {
      throwError(res, err);
    }
    res.render('pages/gallery', {title: 'Gallerie', active: 'gallery', plugins: plugins, message: message});
  });
});

app.get('/plugin/:id/:status*?', function (req, res) {
  var id = req.params.id;
  var status = req.params.status;
  Plugin
  .findById(id, function (err, plugin) {
    if (err) {
      throwError(res, err);
    }
    res.render('pages/plugin', {title: 'Plugin', active: 'plugin', plugin: plugin, status: status});
  });
});

app.get('/load-more/:currentsize', function (req, res) {
  Plugin
  .find({})
  .skip(parseInt(req.params.currentsize))
  .limit(9)
  .exec(function (err, plugins) {
    if (err) {
      throwError(res, err);
    }
    var html = "";
    plugins.forEach(function (plugin) {
      ejs.renderFile('./views/partials/plugin_card_shop.ejs', {plugin: plugin}, function (err, str){
        html += str;
      });
    });
    var pagesize = parseInt(req.params.currentsize) + 9;
    res.send({plugins: html, pagesize: pagesize});
  });
});

app.get('/search-plugin/:name', function (req, res) {
  var name = req.params.name;
  Plugin
  .find({name: {$regex: name, $options: "i"}})
  .exec(function (err, plugins) {
    if (err) {
      throwError(res, err);
    }
    var html = "";
    plugins.forEach(function (plugin) {
      ejs.renderFile('./views/partials/plugin_card_shop.ejs', {plugin: plugin}, function (err, str){
        html += str;
      });
    });
    var pagesize = parseInt(req.params.currentsize) + 9;
    res.send({plugins: html});
  });
});

app.get('/plugin-shop', function (req, res) {
  var pagesize = 9;
  Plugin
  .find({})
  .limit(pagesize)
  .exec(function (err, plugins) {
    if (err) {
      throwError(res, err);
    }
    Plugin
    .countDocuments()
    .exec(function (err, count) {
      if (err) {
        throwError(res, err);
      }
      res.render('pages/shop', {title: 'Shop', active: 'shop', plugins: plugins, pagesize: pagesize});
    });
  });
});

app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('pages/error', {
    title: 'Erreur',
    message: err.message,
    error: {}
  });
});

app.listen(port);

function throwError(res, err) {
  res.render('pages/error', {title: 'Erreur', active: 'error', message: err, error: {}});
}
