document.addEventListener("DOMContentLoaded", function () {

  const upload = new FileUploadWithPreview('pluginImage');

  var imagePreview = document.getElementsByClassName('custom-file-container__image-preview')[0];
  var imageName = document.getElementsByClassName('custom-file-container__custom-file')[0];
  var name = document.getElementById('plgimg').value

  imagePreview.style.backgroundImage = "url('/images/plugins/" + name + "')";
  imageName.value = name;

  document.getElementById('more-plugin-spec').addEventListener('click', function () {
    var row = document.createElement('div');
    row.className = 'form-group row';

    var controlDiv = document.createElement('div');
    controlDiv.setAttribute('class', 'col-3 text-center');

    var defaultDiv = document.createElement('div');
    defaultDiv.setAttribute('class', 'col-3 text-center');

    var minDiv = document.createElement('div');
    minDiv.setAttribute('class', 'col-3 text-center');

    var maxDiv = document.createElement('div');
    maxDiv.setAttribute('class', 'col-3 text-center');

    var controlLabel = document.createElement('label');
    controlLabel.innerHTML = 'Control';

    var defaultLabel = document.createElement('label');
    defaultLabel.innerHTML = 'Default';

    var minLabel = document.createElement('label');
    minLabel.innerHTML = 'Min';

    var maxLabel = document.createElement('label');
    maxLabel.innerHTML = 'Max';

    var controlInput = document.createElement('input');
    controlInput.setAttribute('type', 'text');
    controlInput.setAttribute('name', 'specsControl[]');
    controlInput.setAttribute('class', 'form-control');
    controlInput.setAttribute('placeholder', 'Control');

    var defaultInput = document.createElement('input');
    defaultInput.setAttribute('type', 'text');
    defaultInput.setAttribute('name', 'specsDefault[]');
    defaultInput.setAttribute('class', 'form-control');
    defaultInput.setAttribute('placeholder', 'Default');

    var minInput = document.createElement('input');
    minInput.setAttribute('type', 'text');
    minInput.setAttribute('name', 'specsMin[]');
    minInput.setAttribute('class', 'form-control');
    minInput.setAttribute('placeholder', 'Min');

    var maxInput = document.createElement('input');
    maxInput.setAttribute('type', 'text');
    maxInput.setAttribute('name', 'specsMax[]');
    maxInput.setAttribute('class', 'form-control');
    maxInput.setAttribute('placeholder', 'Max');

    controlDiv.appendChild(controlLabel);
    controlDiv.appendChild(controlInput);

    defaultDiv.appendChild(defaultLabel);
    defaultDiv.appendChild(defaultInput);

    minDiv.appendChild(minLabel);
    minDiv.appendChild(minInput);

    maxDiv.appendChild(maxLabel);
    maxDiv.appendChild(maxInput);

    row.appendChild(controlDiv);
    row.appendChild(defaultDiv);
    row.appendChild(minDiv);
    row.appendChild(maxDiv);

    var div = document.getElementById('plugin-specs');
    div.appendChild(row);
  });
});
