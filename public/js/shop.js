document.addEventListener("DOMContentLoaded", function () {

  addEvent();

  document.getElementById('load-more').addEventListener('click', function () {
    var pagesize = document.getElementById('pagesize-hidden').value;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/load-more/' + pagesize);
    xhr.onload = function () {
      if (xhr.status === 200) {
        var data = JSON.parse(xhr.responseText);
        var plugins = data.plugins;
        var div = document.getElementById('shop-plugins');
        div.innerHTML += plugins;
        document.getElementById('pagesize-hidden').value = data.pagesize;
        addEvent();
      } else {
        alert('Request failed.  Returned status of ' + xhr.status);
      }
    };
    xhr.send();
  });

  document.getElementById('search-button').addEventListener('click', function () {
    var name = document.getElementById('search-plugin-name').value;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/search-plugin/' + name);
    xhr.onload = function () {
      if (xhr.status === 200) {
        document.getElementById('load-more').style.display = 'none';
        var data = JSON.parse(xhr.responseText);
        var plugins = data.plugins;
        var div = document.getElementById('shop-plugins');
        div.innerHTML = plugins;
        addEvent();
      } else {
        alert('Request failed.  Returned status of ' + xhr.status);
      }
    };
    xhr.send();
  });
});

function addEvent() {
  var elements = document.getElementsByClassName('plugin-card-shop');
  for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    element.onclick = function () {
      var id = this.getElementsByClassName('plugin-id')[0].value;
      window.location.href = "/plugin/" + id;
    };
  }
}
