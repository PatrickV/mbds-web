document.addEventListener("DOMContentLoaded", function () {
  var elements = document.getElementsByClassName('plugin-card-shop');
  for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    element.onclick = function () {
      var id = this.getElementsByClassName('plugin-id')[0].value;
      window.location.href = "/plugin/" + id;
    };
  }
});
