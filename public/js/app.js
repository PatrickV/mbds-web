document.addEventListener("DOMContentLoaded", function () {
  if (document.getElementById('logout-button')) {
    document.getElementById('logout-button').addEventListener('click', function () {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', '/logout');
      xhr.onload = function () {
        if (xhr.status === 200) {
          window.location.href = "/gallery";
        }
      };
      xhr.send();
    });
  }
});
