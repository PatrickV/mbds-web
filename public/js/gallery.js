document.addEventListener("DOMContentLoaded", function () {
  var elements = document.getElementsByClassName('plugin-template');
  for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    element.onclick = function () {
      var link = this.getElementsByClassName('plugin-link')[0];
      window.location.href = link.getAttribute('href');
    };
  }
});
