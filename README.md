##Setup
Commande pour importer les plugins dans la base :
- mongoimport -d moddevices -c plugins --jsonArray < plugins.json

Le site est hébergé à l'adresse suivante :
- https://mbds-plugin-shop.herokuapp.com/gallery

En revanche, les images ne sont pas pérsistées, elles sont supprimées à chaque redémarrge du serveur
