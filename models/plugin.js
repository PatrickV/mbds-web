var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PluginSchema = new Schema({
  name: String,
  author_link: String,
  image: String,
  author: String,
  usage: Number,
  desc_rows: [{control: String, default: String, min: String, max: String}],
  desc_header: {control: String, default: String, min: String, max: String},
  version: String,
  inspired_by: String,
  desc: String,
  tags: [{type: String}]
});

module.exports = mongoose.model('Plugin', PluginSchema);
