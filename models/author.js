const bcrypt = require('bcrypt');
const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ObjectId = mongoose.Schema.Types.ObjectId;

var AuthorSchema = new Schema({
  name: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  url: String,
  plugins: [{type: ObjectId}]
});

AuthorSchema.pre('save', function (next) {
  var author = this;
  if (!author.isModified('password')) return next();
  bcrypt.genSalt(10, function(err, salt) {
    if (err) {
      return next(err);
    } else {
      bcrypt.hash(author.password, salt, function(err, hash) {
        if (err) {
          return next(err);
        } else {
          author.password = hash;
          next();
        }
      });
    }
  });
});

AuthorSchema.methods.comparePassword = function(password, cb) {
  bcrypt.compare(password, this.password, function(err, result) {
    if (err) {
      return cb(err);
    } else {
      cb(null, result);
    }
  });
};

module.exports = mongoose.model('Author', AuthorSchema);
