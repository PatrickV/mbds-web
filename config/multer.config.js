const multer = require('multer');

var storage = multer.diskStorage({
	destination: (req, file, cb) => {
		if (file !== undefined) {
			cb(null, 'public/images/plugins/');
		}
	},
	filename: (req, file, cb) => {
		if (file !== undefined) {
			var ext = file.originalname;
	    var exts = ext.split('.');
	    var name = req.body.pluname.split(' ').join('_');
		  cb(null, req.body.pluname + '-600x450cf23.' + exts[exts.length - 1]);
		}
	}
});

var upload = multer({storage: storage});

module.exports = upload;
